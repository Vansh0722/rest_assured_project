package demoTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AssuredTest {
    @Test
    public void checkNotNull(){
        String ipResponse = RestAssured.given().when().get("https://api.ipify.org/?format=json").thenReturn().asString();
        System.out.println(ipResponse);
        Assert.assertNotNull(ipResponse);
    }
    @Test
    public void GetBookDetails()
    {
        // Specify the base URL to the RESTful web service
        RestAssured.baseURI = "https://demoqa.com/BookStore/v1/Books";
        // Get the RequestSpecification of the request to be sent to the server
        RequestSpecification httpRequest = RestAssured.given();

        Response response = httpRequest.get("");

        // Get the status code of the request.
        //If request is successful, status code will be 200
        int statusCode = response.getStatusCode();

        // Assert that correct status code is returned.
        Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/,
                "Correct status code returned");

    }
}
