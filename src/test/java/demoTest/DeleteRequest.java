package demoTest;

import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class DeleteRequest {
    @Test
    public void patchRequest() {
        given().
                when().
                delete("https://reqres.in/api/users/2").
                then().
                statusCode(204);
    }

}
