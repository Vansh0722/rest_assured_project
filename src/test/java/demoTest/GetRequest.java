package demoTest;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class GetRequest {

    @Test
    public void test_1(){
        given().get("https://reqres.in/api/users?page=2").
                then().
                log().all().
                statusCode(200).
//                body("data.id[0]", equalTo(7)).
                body("data.first_name", hasItems("Michael"));
    }

}
