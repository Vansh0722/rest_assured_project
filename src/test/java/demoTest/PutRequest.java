package demoTest;

import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PutRequest {
    @Test
    public void test_1() {
        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("name","Raghav");
//        map.put("job", "Developer");
//        System.out.println(map);

        JSONObject request = new JSONObject(map);
        request.put("name", "Vansh");
        request.put("job", "Developer");
        System.out.println(request);
        System.out.println(request.toJSONString());
        given().
                header("content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                put("https://reqres.in/api/users/2").
                then().
                statusCode(200).
                log().
                all();
    }


}
